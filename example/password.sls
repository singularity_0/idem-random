random_state01011:
    random.password.present:
        - name: rs01011
        - length: 13
        - keepers:
            key: val

always-changes-trigger01101:
    trigger.build:
        - triggers:
             - comment: ${random.password:random_state01011:output}
